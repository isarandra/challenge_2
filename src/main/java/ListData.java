public abstract class ListData {
    public abstract void setListInput(int[] input);
    public abstract void printInputList();
    public abstract void sort(int[] list);
}
