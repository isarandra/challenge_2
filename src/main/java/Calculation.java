import java.io.*;
import java.util.*;

public class Calculation extends ListData implements Mean, Median, Modus, ReadCsv, Klasifikasi{
    private int[] input;
    private int count = 0; //for recursive at sort method;
    private int[] inputObject;

    @Override
    public void mean(int[] input) {
        int count=0;
        double sum=0;
        Double mean=0.0;
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter("meanMedian.txt"));
            for(double number:input){
                sum+=number;
                count++;
            }
            if (count != 0) {
                mean = sum/count;
//                System.out.print("Mean: "+mean);
                writer.write("Mean: ");
                writer.write(mean.toString());
                writer.close();
            }
//            System.out.println(" ");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
    @Override
    public void modus(int[] input) {
        Map<Integer,Integer> numberAppearance = new HashMap<>();

        for(int number:input){
            if(numberAppearance.containsKey(number)){
                numberAppearance.computeIfPresent(number,(key, val)->val+1);
            }
            else{
                numberAppearance.computeIfAbsent(number,k->1);
            }
        }

        int modusValue = -1;
        Integer modusKey=0;
        int numberOfModus=0;
        List<Integer> modusKeyList=new ArrayList<>();
        for(Map.Entry<Integer, Integer> number:numberAppearance.entrySet()){
            if(number.getValue()>modusValue){
                modusValue=number.getValue();
                modusKey=number.getKey();
            }
        }
        for(Map.Entry<Integer, Integer> number:numberAppearance.entrySet()){
            if(number.getValue()==modusValue){
                modusKeyList.add(number.getKey());
                numberOfModus++;
            }
        }
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter("modus.txt"));
            if(numberOfModus>1){
                //for output txt
                int[] modusKeyListInt = modusKeyList.stream().mapToInt(i->i).toArray();
                String[] modusKeyListString = Arrays.stream(modusKeyListInt).mapToObj(String::valueOf).toArray(String[]::new);
                writer.write("Modus: ");
                for(int i=0;i<modusKeyListString.length;i++){
                    writer.write(modusKeyListString[i]);
                    if(i!=modusKeyListString.length-1){
                        writer.write(", ");
                    }
                }
                writer.close();
            }
            else {
                writer.write("Modus: ");
                writer.write(modusKey.toString());
                writer.close();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    @Override
    public void median(int[] input) {
        double medianIndex;
        Double median;
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter("meanMedian.txt", true));
            if(input.length%2==0){
                median=(input[input.length/2-1]+input[input.length/2])/2.0;
                writer.write("\nMedian: ");
                writer.write(median.toString());
                writer.close();
            }
            else{
                medianIndex=input.length/2.0;
                median=(double)input[(int)medianIndex];
                writer.write("\nMedian: ");
                writer.write(median.toString());
                writer.close();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    @Override
    public void setListInput(int[] input) {
        this.input = input;
    }
    @Override
    public void printInputList() {
        System.out.print("Input: ");
        System.out.print("[");
        for (int i=0;i<input.length;i++){
            if (i==input.length-1){
                System.out.print(input[i]);
            }
            else{
                System.out.print(input[i] + ", ");
            }
        }
        System.out.print("]");
    }
    @Override
    public void sort(int[] list) {
        int dummy = 0;
        if (count == list.length){
            return;
        }
        for(int i=1;i<list.length;i++){
            if(list[i-1]>list[i]){
                dummy=list[i-1];
                list[i-1]=list[i];
                list[i]=dummy;
            }
        }
        count++;
        sort(list);
    }
    @Override
    public int[] readCsv(String file) {
        BufferedReader reader = null;
        String line = "";
        ArrayList<Integer> inputArray = new ArrayList<>();

        try{
            reader = new BufferedReader(new FileReader(file));
            while((line = reader.readLine()) != null){

                String[] row = line.split(";");

                for(int i=1;i<row.length;i++){
                    inputArray.add(Integer.parseInt(row[i]));
                }
            }
            inputObject = inputArray.stream().mapToInt(i->i).toArray();
        }
        catch (Exception e){
        }
        finally {
            try{
                reader.close();
            }
            catch (Exception e){
            }
        }
        return inputObject;
    }
    @Override
    public void klasifikasi(int[] input){
        Integer countLessThanSix=0;
        for(int number:input){
            if(number<6){
                countLessThanSix++;
            }
        }
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter("klasifikasi.txt"));
            writer.write("Nilai  | Frekuensi\n");
            writer.write("< 6");
            writer.write("    |     ");
            writer.write(countLessThanSix.toString());
            Map<Integer,Integer> numberAppearance = new HashMap<>();

            for(int number:input){
                if(numberAppearance.containsKey(number)){
                    numberAppearance.computeIfPresent(number,(key, val)->val+1);
                }
                else{
                    numberAppearance.computeIfAbsent(number,k->1);
                }
            }

            Map<Integer, Integer> filteringKurangDariTujuh = new LinkedHashMap<>();

            for(Map.Entry<Integer,Integer> number:numberAppearance.entrySet()){
                if(number.getKey() >=6){
                    filteringKurangDariTujuh.put(number.getKey(),number.getValue());
                }
            }
            System.out.println();
            for(Map.Entry<Integer, Integer> number:filteringKurangDariTujuh.entrySet()){
                writer.write("\n");
                writer.write(number.getKey().toString());
                writer.write("      |     ");
                writer.write(number.getValue().toString());
            }
            writer.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
